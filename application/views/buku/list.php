<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">

        <h2><?php echo $title ?></h2>
        <p>
          <a class="btn btn-success btn-lg pull-right" href="<?php echo base_url('katalog')?>"><i class="glyphicon glyphicon-search"></i> Pencarian Buku </a>
        </p>


        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>No.</th>
                <th>Cover</th>
                <th>Judul Buku</th>
                <th>Penulis Buku</th>
                <th width="5%">Jumlah Buku</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach ($buku as $buku): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td class="text-center">
                    <?php if (empty($buku->cover_buku)): ?>
                      Tidak Ada Cover
                    <?php else: ?>
                      <img src="<?php echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku); ?>" class="img img-thumbnail" width="60">
                    <?php endif; ?>
                  </td>
                  <td><?php echo $buku->judul_buku; ?></td>
                  <td><?php echo $buku->penulis_buku; ?></td>
                  <td><?php echo $buku->jumlah_buku; ?></td>
                  <td class="text-center">
                    <a href="<?php echo base_url('katalog/read/'.$buku->id_buku); ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-eye-open"></i> Baca </a>
                  </td>
                </tr>
                <?php $i++; endforeach; ?>
              </tbody>
            </table>
        </div>


      </div>
    </div>
  </div>
</div>
