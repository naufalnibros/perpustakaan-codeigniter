<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title><?php echo $title ?></title>
    <link href="<?php echo base_url('assets/bootstrap4/')?>dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/template/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/template/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/template/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/bootstrap4/')?>style.css" rel="stylesheet">
    <!-- ViewerJs -->
    <script src="<?php echo base_url('assets/plugins/viewerjs/pdf.js')?>" charset="utf-8"></script>
    <script src="<?php echo base_url('assets/plugins/viewerjs/pdfjsversion.js')?>" charset="utf-8"></script>
    <script src="<?php echo base_url('assets/plugins/viewerjs/pdf.worker.js')?>" charset="utf-8"></script>
  </head>

  <body>

    <div class="container">
