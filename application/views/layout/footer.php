<!-- Site footer -->
<footer class="footer">
  <p>&copy; NAUFAL IM <?php echo date('Y') ?></p>
</footer>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/jquery/jquery.min.js')?>" charset="utf-8"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/bootstrap4/')?>assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url('assets/bootstrap4/')?>assets/js/vendor/popper.min.js"></script>
<script src="<?php echo base_url('assets/bootstrap4/')?>dist/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="<?php echo base_url('assets/bootstrap4/')?>assets/js/vendor/holder.min.js"></script>
<script src="<?php echo base_url('assets/template')?>/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/template')?>/assets/js/dataTables/dataTables.bootstrap.js"></script>

<script>
  $(document).ready(function () {
    $('#dataTables-example').dataTable();
  });
</script>

</body>
</html>
