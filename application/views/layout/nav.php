<nav class="navbar navbar-expand-md navbar-light bg-light rounded mb-3">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav text-md-center nav-justified w-100">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('katalog')?>">Katalog Buku</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('berita')?>">Berita</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('buku')?>">Buku Baru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('usulan')?>">Request Buku</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('kontak')?>">Kontak</a>
      </li>
    </ul>
  </div>
</nav>
</header>
