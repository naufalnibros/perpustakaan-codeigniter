<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <h3><?php echo $title; ?></h3>
        <?php if ($this->session->flashdata('sukses')): ?>
          <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('sukses'); ?>
          </div>
        <?php else: ?>
          <p class="alert alert-success">Anda dapat mengusulkan koleksi buku terbaru melalui formulis dibawah ini. Silahkan masukkan data lengkap usulan buku baru Anda</p>

        <?php
          echo validation_errors('<div class="alert alert-warning">','</div>');
          echo form_open(base_url('usulan'));
         ?>

         <div class="form-group">
           <div class="col-md-4">
             Judul Buku Baru <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <input class="form-control" type="text" name="judul" value="<?php echo set_value('judul')?>" placeholder="Judul Buku Baru" required>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4">
             Nama Penulis <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <input class="form-control" type="text" name="penulis" value="<?php echo set_value('penulis')?>" placeholder="Nama Penulis" required>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4">
             Nama Penerbit <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <input class="form-control" type="text" name="penerbit" value="<?php echo set_value('penerbit')?>" placeholder="Nama Penerbit" required>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4">
             Keterangan <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <textarea name="keterangan" rows="8" cols="80" class="form-control"><?php echo set_value('keterangan') ?></textarea>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4">
             Nama Pengusul <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <input class="form-control" type="text" name="nama_pengusul" value="<?php echo set_value('nama_pengusul')?>" placeholder="Nama Pengusul" required>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4">
             Email Pengusul <span class="text-danger">*</span>
           </div>
           <div class="col-md-8">
             <input class="form-control" type="email" name="email_pengusul" value="<?php echo set_value('email_pengusul')?>" placeholder="name@email.com" required>
           </div>
         </div>

         <div class="col-md-12"><hr></div>
         <div class="form-group">
           <div class="col-md-4"></div>
           <div class="col-md-8">
             <input type="submit" name="submit" value="Kirim Usulan" class="btn btn-success btn-lg">
             <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
           </div>
         </div>

         <?php echo form_close(); ?>
       <?php endif; ?>

      </div>
    </div>
  </div>
</div>
