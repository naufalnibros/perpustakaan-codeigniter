<main role="main">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php $a=0; foreach ($slider as $slide): ?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $a; ?>" class="<?php if($a === 0){echo "active";}?>"></li>
      <?php $a++; endforeach; ?>
    </ol>
    <div class="carousel-inner">

    <?php $i=1; foreach ($slider as $slider): ?>
      <div class="carousel-item <?php if($i === 1){echo "active";}?>">
        <img class="first-slide" src="<?php echo base_url('assets/upload/image/'.$slider->gambar);?>" alt="<?php echo $slider->judul_berita?>">
        <div class="container">
          <div class="carousel-caption text-left">
            <h1><?php echo $slider->judul_berita ?></h1>
            <p><?php echo character_limiter($slider->isi, 100) ?></p>
            <p><a class="btn btn-lg btn-primary" href="<?php echo base_url('berita/read/'.$slider->slug_berita) ?>" role="button">Baca Selengkapnya</a></p>
          </div>
        </div>
      </div>
    <?php $i++; endforeach; ?>


    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</main>

<hr class="featurette-divider">
<!-- Example row of columns -->
<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <img src="<?php echo base_url('assets/upload/image/thumbs/'.$berita->gambar)?>" alt="<?php echo $berita->judul_berita?>" class="img img-responsive img-thumbnail">
          </div>
          <div class="col-md-8 post">
            <h2><a href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>"><?php echo $berita->judul_berita ?></a></h2>
            <p><?php echo character_limiter($berita->isi, 200); ?></p>
            <p class="text-right"><a class="btn btn-sm btn-primary" href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>">Baca Selengkapnya</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-4">
    <div class="sidebar-module sidebar-module-inset">
      <div class="panel panel-default">
        <div class="panel-body">
          <h2>Pencarian Buku</h2>
          <form class="form-inline text-center" action="<?php echo base_url('katalog')?>" method="post">
            <input type="text" name="cari" placeholder="Cari judul buku disini!" required>
            <input type="submit" name="submit" value="Cari">
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-8">
    <div class="panel panel-default">
      <div class="panel-body">
        <h2>Data Perpustakaan Online</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
      </div>
    </div>
  </div>

  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-body">
        <h2>Buku Baru</h2>

        <?php foreach ($buku as $buku): ?>
        <div class="row buku">
          <div class="col-md-4">
            <a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>">
              <img class="img-thumbnail img-rounded" src="<?php echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku)?>" alt="<?php echo $buku->judul_buku?>">
            </a>
          </div>
          <div class="col-md-8">
            <h4><a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>"><?php echo $buku->judul_buku ?></a></h4>
            <p class="text-justify"><?php echo character_limiter($buku->ringkasan, 60); ?></p>
          </div>
          <div class="clearfix"></div>

        </div>
        <?php endforeach; ?>

        <p>
          <a class="btn btn-primary btn-block" href="<?php echo base_url('katalog')?>">Koleksi Buku</a>
        </p>

      </div>
    </div>
  </div>

</div>
