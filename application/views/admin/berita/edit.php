<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

if (isset($error)) {
  echo "<div class='alert alert-warning'>";
  echo $error;
  echo "</div>";
}

//open form
echo form_open_multipart(base_url('admin/berita/edit/'.$berita->id_berita));

 ?>

<div class="col-md-12">
  <div class="form-group">
    <label>Judul Berita</label>
    <input class="form-control" type="text" name="judul_berita" value="<?php echo $berita->judul_berita ?>" placeholder="Judul Berita" required>
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Status Berita</label>
    <select class="form-control" name="status_berita">
      <option value="Publish">Publikasikan</option>
      <option value="Draft" <?php if($berita->status_berita == 'Draft'){echo "selected";} ?>>Simpan Sebagai Draft</option>
    </select>
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Jenis Berita</label>
    <select class="form-control" name="jenis_berita">
      <option value="Berita">Berita</option>
      <option value="Slider" <?php if($berita->jenis_berita == 'Slider'){echo "selected";} ?>>Homepage Slider</option>
    </select>
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Upload Gambar</label>
    <input type="file" name="gambar" placeholder="gambar" class="form-control">
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Isi Berita</label>
    <textarea name="isi" class="form-control textarea" placeholder="Isi Berita"><?php echo $berita->isi ?></textarea>
  </div>
</div>

<div class="col-md-12 text-center">
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
