<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open(base_url('admin/anggota/tambah'));

 ?>

<div class="col-md-6">
  <div class="form-group">
    <label>Nama</label>
    <input class="form-control" type="text" name="nama_anggota" value="<?php echo set_value('nama_anggota'); ?>" placeholder="Nama Anggota" required>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email" required>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input class="form-control" type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder="Username" required>
  </div>
  <div class="form-group">
    <label>Password</label>
    <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password" required>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label>Telepon</label>
    <input class="form-control" type="text" name="telepon" value="<?php echo set_value('telepon'); ?>" placeholder="Telepon" required>
  </div>
  <div class="form-group">
    <label>Status Anggota</label>
    <select class="form-control" name="status_anggota">
      <option value="Active">Active</option>
      <option value="Non Active">Non Active</option>
    </select>
  </div>
  <div class="form-group">
    <label>Nama Instansi</label>
    <textarea name="instansi" class="form-control" placeholder="Nama Instansi"><?php echo set_value('instansi'); ?></textarea>
  </div>
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
