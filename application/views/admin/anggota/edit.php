<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open(base_url('admin/anggota/edit/'.$anggota->id_anggota));

 ?>

<div class="col-md-6">
  <div class="form-group">
    <label>Nama</label>
    <input class="form-control" type="text" name="nama_anggota" value="<?php echo $anggota->nama_anggota ?>" placeholder="Nama" required>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input class="form-control" type="email" name="email" value="<?php echo $anggota->email ?>" placeholder="Email" required>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input class="form-control" type="text" name="username" value="<?php echo $anggota->username ?>" placeholder="Username" required readonly disabled>
  </div>
  <div class="form-group">
    <label>Password <span class="text-danger"><small>(Password minimal 6 karakter atau biarkan kosong!)</small></span></label>
    <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password">
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label>telepon</label>
    <input class="form-control" type="text" name="telepon" value="<?php echo $anggota->telepon ?>" placeholder="Telepon" required>
  </div>
  <div class="form-group">
    <label>Status Anggota</label>
    <select class="form-control" name="status_anggota">
      <option value="Active" <?php if ($anggota->status_anggota == "Active") { echo "selected"; } ?>>Active</option>
      <option value="Non Active" <?php if ($anggota->status_anggota == "Non Active") { echo "selected"; } ?>>Non Active</option>
    </select>
  </div>
  <div class="form-group">
    <label>Nama Instansi</label>
    <textarea name="instansi" class="form-control" placeholder="Nama Instansi"><?php echo $anggota->instansi ?></textarea>
  </div>
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
