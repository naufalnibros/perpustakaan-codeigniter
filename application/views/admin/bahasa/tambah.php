<p>
  <button class="btn btn-success" data-toggle="modal" data-target="#TambahBahasa">
    <i class="fa fa-fa-plus"></i> Tambah
  </button>
</p>

<div class="modal fade" id="TambahBahasa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"> Tambah Data Baru</h4>
      </div>
      <div class="modal-body">



        <?php
        //notifikasi kalau ada input error
        echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

        //open form
        echo form_open(base_url('admin/bahasa'));

        ?>

        <div class="col-md-6">
          <div class="form-group">
            <label>Nama Bahasa Buku</label>
            <input class="form-control" type="text" name="nama_bahasa" value="<?php echo set_value('nama_bahasa'); ?>" placeholder="Nama Bahasa Buku" required>
          </div>
          <div class="form-group">
            <label>Kode Bahasa Buku</label>
            <input class="form-control" type="text" name="kode_bahasa" value="<?php echo set_value('kode_bahasa'); ?>" placeholder="Kode Bahasa" required>
          </div>
          <div class="form-group">
            <label>Urutan</label>
            <input class="form-control" type="number" name="urutan" value="<?php echo set_value('urutan'); ?>" placeholder="Nomor Urut Tampil" required>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control" placeholder="Keterangan"><?php echo set_value('keterangan'); ?></textarea>
          </div>
          <div class="form-group">
            <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
            <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
          </div>
        </div>

        <?php
        echo form_close();
        ?>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
      </div>
    </div>
  </div>
</div>
