<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open(base_url('admin/bahasa/edit/'.$bahasa->id_bahasa));

 ?>

<div class="col-md-6">
  <div class="form-group">
    <label>Nama Bahasa Buku</label>
    <input class="form-control" type="text" name="nama_bahasa" value="<?php echo $bahasa->nama_bahasa ?>" placeholder="Nama Bahasa Buku" required>
  </div>
  <div class="form-group">
    <label>Kode Bahasa Buku</label>
    <input class="form-control" type="text" name="kode_bahasa" value="<?php echo $bahasa->kode_bahasa ?>" placeholder="Kode Bahasa Buku" required>
  </div>
  <div class="form-group">
    <label>Urutan</label>
    <input class="form-control" type="number" name="urutan" value="<?php echo $bahasa->urutan ?>" placeholder="Urutan Tampil" required>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="keterangan" class="form-control" placeholder="Keterangan"><?php echo $bahasa->keterangan ?></textarea>
  </div>
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
