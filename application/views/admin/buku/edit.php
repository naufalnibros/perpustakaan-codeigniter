<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

if (isset($error)) {
  echo "<div class='alert alert-warning'>";
  echo $error;
  echo "</div>";
}

//open form
echo form_open_multipart(base_url('admin/buku/edit/'.$buku->id_buku));

 ?>

<div class="col-md-12">
  <div class="form-group">
    <label>Judul Buku</label>
    <input class="form-control" type="text" name="judul_buku" value="<?php echo $buku->judul_buku; ?>" placeholder="Judul Buku" required>
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Penulis Buku</label>
    <input class="form-control" type="text" name="penulis_buku" value="<?php echo $buku->penulis_buku; ?>" placeholder="Penulis Buku" required>
  </div>
  <div class="form-group">
    <label>Jenis Buku</label>
    <select class="form-control" name="id_jenis" required>
      <?php foreach ($jenis as $jenis): ?>
        <option value="<?php echo $jenis->id_jenis?>"  <?php if($buku->id_jenis == $jenis->id_jenis){echo "selected";} ?>><?php echo $jenis->kode_jenis ?> - <?php echo $jenis->nama_jenis ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div class="form-group">
    <label>Bahasa Buku</label>
    <select class="form-control" name="id_bahasa" required>
      <?php foreach ($bahasa as $bahasa): ?>
        <option value="<?php echo $bahasa->id_bahasa?>"  <?php if($buku->id_bahasa == $bahasa->id_bahasa){echo "selected";} ?>><?php echo $bahasa->kode_bahasa ?> - <?php echo $bahasa->nama_bahasa ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div class="form-group">
    <label>Subjek Buku</label>
    <input class="form-control" type="text" name="subjek_buku" value="<?php echo $buku->subjek_buku; ?>" placeholder="Subjek Buku">
  </div>
  <div class="form-group">
    <label>Letak Buku</label>
    <input class="form-control" type="text" name="letak_buku" value="<?php echo $buku->letak_buku; ?>" placeholder="Letak Buku">
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Kode Buku</label>
    <input class="form-control" type="text" name="kode_buku" value="<?php echo $buku->kode_buku; ?>" placeholder="Kode Buku">
  </div>
  <div class="form-group">
    <label>Kolasi</label>
    <input class="form-control" type="text" name="kolasi" value="<?php echo $buku->kolasi; ?>" placeholder="Kolasi">
  </div>
  <div class="form-group">
    <label>Penerbit</label>
    <input class="form-control" type="text" name="penerbit" value="<?php echo $buku->penerbit; ?>" placeholder="Penerbit">
  </div>
  <div class="form-group">
    <label>Tahun Terbit</label>
    <input class="form-control" type="number" name="tahun_terbit" value="<?php echo $buku->tahun_terbit; ?>" placeholder="Tahun Terbit">
  </div>
  <div class="form-group">
    <label>Nomor Seri</label>
    <input class="form-control" type="text" name="nomor_seri" value="<?php echo $buku->nomor_seri; ?>" placeholder="Nomor Seri">
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label>Status Buku</label>
    <select class="form-control" name="status_buku">
      <option value="Publish" <?php if($buku->status_buku == "Publish"){ echo "selected";} ?>>Publish</option>
      <option value="Not Publish" <?php if($buku->status_buku == "Not Publish"){ echo "selected";} ?>>Not Publish</option>
      <option value="Missing" <?php if($buku->status_buku == "Missing"){ echo "selected";} ?>>Missing</option>
    </select>
  </div>
  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" class="form-control" placeholder="Ringkasan"><?php echo $buku->ringkasan; ?></textarea>
  </div>
  <div class="form-group">
    <label>Jumlah Buku</label>
    <input class="form-control" type="number" name="jumlah_buku" value="<?php echo $buku->jumlah_buku; ?>" placeholder="Jumlah Buku" required>
  </div>
  <div class="form-group">
    <label>Cover Buku (<span class="text-warning">atau biarkan kosong</span>)</label>
    <input class="form-control" type="file" name="cover_buku" value="<?php echo $buku->cover_buku; ?>">
    <br>
    <?php if (empty($buku->cover_buku)): ?>
      <span class="text-danger"><small>Belum ada data yang diupload</small></span>
    <?php else: ?>
      <img src="<?php echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku);?>" class="img img-thumbnail" width="60">
    <?php endif; ?>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
