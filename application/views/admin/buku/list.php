<p><a href="<?php echo base_url('admin/buku/tambah');?>" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah Buku</a></p>

<?php
  //notofikasi -> tambah data
  if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo "</div>";
  }
 ?>

 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
   <thead>
     <tr>
       <th>No.</th>
       <th>Cover</th>
       <th>Judul Buku</th>
       <th>Penulis Buku</th>
       <th>Jumlah Buku</th>
       <th width="10%">Jenis - Bahasa</th>
       <th>File</th>
       <th width="25%">Action</th>
     </tr>
   </thead>
   <tbody>
   <?php $i=1; foreach ($buku as $buku): ?>
     <?php
       $id_buku = $buku->id_buku;
       $file_buku = $this->file_buku_model->buku($id_buku);
      ?>
     <tr>
       <td><?php echo $i; ?></td>
       <td>
       <?php if (empty($buku->cover_buku)): ?>
         Tidak Ada Cover
       <?php else: ?>
         <img src="<?php echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku); ?>" class="img img-thumbnail" width="60">
       <?php endif; ?>
       </td>
       <td><?php echo $buku->judul_buku; ?></td>
       <td><?php echo $buku->penulis_buku; ?></td>
       <td><?php echo $buku->jumlah_buku; ?></td>
       <td><?php echo $buku->kode_jenis.' - '.$buku->kode_bahasa; ?></td>
       <td><?php echo count($file_buku) ?> File</td>
       <td>
         <a href="<?php echo base_url('admin/file_buku/kelola/'.$buku->id_buku); ?>" class="btn btn-info btn-xs"><i class="fa fa-book"></i> Kelola File</a>
         <?php include 'detail.php'; ?>
         
         <a href="<?php echo base_url('admin/buku/edit/'.$buku->id_buku); ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
         <?php include 'delete.php'; ?>
       </td>
     </tr>
   <?php $i++; endforeach; ?>
   </tbody>
 </table>
