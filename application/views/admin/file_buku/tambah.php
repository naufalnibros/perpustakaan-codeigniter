<p>
  <button class="btn btn-success" data-toggle="modal" data-target="#uploadFile">
    <i class="fa fa-upload"></i> Upload File Baru
  </button>
</p>
<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

if (isset($error)) {
  echo "<div class='alert alert-warning'>";
  echo $error;
  echo "</div>";
}
?>


<?php
//open form
echo form_open_multipart(base_url('admin/file_buku/kelola/'.$buku->id_buku));
 ?>

 <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title" id="myModalLabel"> Tambah File Baru</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
           <label for="">Judul File</label>
           <input type="text" class="form-control" name="judul_file" placeholder="Judul File" value="<?php echo set_value('judul_file')?>" required>
         </div>
         <div class="form-group">
           <label for="">Upload File</label>
           <input type="file" class="form-control" name="nama_file" required>
         </div>
         <div class="form-group">
           <label for="">Urutan File</label>
           <input type="number" class="form-control" name="urutan" placeholder="Urutan File" value="<?php echo set_value('urutan')?>">
         </div>
         <div class="form-group">
           <label for="">Keterangan File</label>
           <textarea name="keterangan" class="form-control" placeholder="Keterangan File"><?php echo set_value('keterangan') ?></textarea>
         </div>

       </div>
       <div class="modal-footer">
         <input type="submit" name="submit" value="Upload File Baru" class="btn btn-success">
         <input type="reset" name="reset" value="Reset" class="btn btn-default">
         <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
       </div>
     </div>
   </div>
 </div>

 <?php
 echo form_close();
  ?>
