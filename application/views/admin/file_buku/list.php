<?php if($this->uri->segment(3) != ""): ?>
  <?php include 'tambah.php'; ?>
<?php else: ?>
  <p><a href="<?php echo base_url('admin/buku');?>" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah File Buku</a></p>
<?php endif; ?>

<?php
  //notofikasi -> tambah data
  if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo "</div>";
  }
 ?>

 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
   <thead>
     <tr>
       <th>No.</th>
       <th width="25%">Judul Buku</th>
       <th>Nama File</th>
       <th>Urutan</th>
       <th>Keterangan</th>
       <th width="25%">Action</th>
     </tr>
   </thead>
   <tbody>
   <?php $i=1; foreach ($file_buku as $file_buku): ?>
     <tr>
       <td><?php echo $i; ?></td>
       <td><?php echo $file_buku->judul_file; ?><br>
         <small>
           Judul :  <?php echo $file_buku->judul_buku; ?>
         </small>
       </td>
       <td><?php echo $file_buku->nama_file; ?></td>
       <td><?php echo $file_buku->urutan; ?></td>
       <td><?php echo $file_buku->keterangan; ?></td>
       <td>
         <a href="<?php echo base_url('admin/file_buku/unduh/'.$file_buku->id_file_buku); ?>" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i> Unduh</a>
         <a href="<?php echo base_url('admin/file_buku/edit/'.$file_buku->id_file_buku); ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
         <?php include 'delete.php'; ?>
       </td>
     </tr>
   <?php $i++; endforeach; ?>
   </tbody>
 </table>
