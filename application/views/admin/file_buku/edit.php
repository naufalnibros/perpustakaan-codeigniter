<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

if (isset($error)) {
  echo "<div class='alert alert-warning'>";
  echo $error;
  echo "</div>";
}

//open form
echo form_open_multipart(base_url('admin/file_buku/edit/'.$file_buku->id_file_buku));
?>


 <div class="form-group">
   <label for="">Judul File</label>
   <input type="text" class="form-control" name="judul_file" placeholder="Judul File" value="<?php echo $file_buku->judul_file ?>" required>
 </div>
 <div class="form-group">
   <label for="">Upload File <small>( file lama: <a href="<?php echo base_url('admin/file_buku/unduh/'.$file_buku->id_file_buku)?>" target="_blank"><i class="fa fa-download"></i><?php echo $file_buku->nama_file ?></a> )</small></label>
   <input type="file" class="form-control" name="nama_file">
 </div>
 <div class="form-group">
   <label for="">Urutan File</label>
   <input type="number" class="form-control" name="urutan" placeholder="Urutan File" value="<?php echo $file_buku->urutan?>">
 </div>
 <div class="form-group">
   <label for="">Keterangan File</label>
   <textarea name="keterangan" class="form-control" placeholder="Keterangan File"><?php echo $file_buku->keterangan ?></textarea>
 </div>
 <div class="form-group">
   <input type="submit" name="submit" value="Update Data File" class="btn btn-success">
   <input type="reset" name="reset" value="Reset" class="btn btn-default">
 </div>

 <?php echo form_close(); ?>
