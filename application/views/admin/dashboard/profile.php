<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//nofikasi
if ($this->session->flashdata('sukses')) {
  echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
  echo $this->session->flashdata('sukses');
  echo "</div>";
}

//open form
echo form_open(base_url('admin/dashboard/profile'));

 ?>

<div class="col-md-6">
  <div class="form-group">
    <label>Nama</label>
    <input class="form-control" type="text" name="nama" value="<?php echo $user->nama ?>" placeholder="Nama" required>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input class="form-control" type="email" name="email" value="<?php echo $user->email ?>" placeholder="Email" required>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input class="form-control" type="text" name="username" value="<?php echo $user->username ?>" placeholder="Username" required readonly disabled>
  </div>
  <div class="form-group">
    <label>Password <span class="text-danger"><small>(Password minimal 6 karakter atau biarkan kosong!)</small></span></label>
    <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password">
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label>Hak Akses Level</label>
    <select class="form-control" name="akses_level">
      <option value="<?php echo $user->akses_level;?>"><?php echo $user->akses_level; ?></option>
    </select>
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="keterangan" class="form-control" placeholder="Keterangan"><?php echo $user->keterangan ?></textarea>
  </div>
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
