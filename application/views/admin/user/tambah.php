<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open(base_url('admin/user/tambah'));

 ?>

<div class="col-md-6">
  <div class="form-group">
    <label>Nama</label>
    <input class="form-control" type="text" name="nama" value="<?php echo set_value('nama'); ?>" placeholder="Nama" required>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email" required>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input class="form-control" type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder="Username" required>
  </div>
  <div class="form-group">
    <label>Password</label>
    <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password" required>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label>Hak Akses Level</label>
    <select class="form-control" name="akses_level">
      <option value="Admin">Admin</option>
      <option value="User">User</option>
    </select>
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="keterangan" class="form-control" placeholder="Keterangan"><?php echo set_value('keterangan'); ?></textarea>
  </div>
  <div class="form-group">
    <input type="submit" name="submit" value="Simpan Data" class="btn btn-success btn-lg">
    <input type="reset" name="reset" value="Reset" class="btn btn-default btn-lg">
  </div>
</div>

 <?php
 echo form_close();
  ?>
