<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-8 post">
            <h2><?php echo $berita->judul_berita ?></h2>
            <img src="<?php echo base_url('assets/upload/image/'.$berita->gambar)?>" alt="<?php echo $berita->judul_berita?>" class="img img-responsive img-thumbnail">
            <hr>
            <?php echo $berita->isi ?>
          </div>
          <div class="col-md-4 list-berita">
            <h2>Berita lainnya</h2>
            <?php foreach ($list_berita as $list_berita): ?>
            <ul>
              <li><a href="<?php echo base_url('berita/read/'.$list_berita->slug_berita)?>"><?php echo $list_berita->judul_berita?></a></li>
            </ul>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
