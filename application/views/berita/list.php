<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">


        <div class="row">
          <div class="col-md-4">
            <img src="<?php echo base_url('assets/upload/image/thumbs/'.$berita->gambar)?>" alt="<?php echo $berita->judul_berita?>" class="img img-responsive img-thumbnail">
          </div>
          <div class="col-md-8 post">
            <h2><a href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>"><?php echo $berita->judul_berita ?></a></h2>
            <p><?php echo character_limiter($berita->isi, 400); ?></p>
            <p class="readmore"><a class="btn btn-sm btn-primary" href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>">Baca Selengkapnya</a></p>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>
