<div class="row">
  <div class="col-lg-12">
    <div class="sidebar-module sidebar-module-inset">
      <div class="panel">
        <div class="panel-body panel-success">
          <form class="form-inline" action="<?php echo base_url('katalog')?>" method="post">
            <input type="text" class="form-control" name="cari" placeholder="Cari buku disini!" required>
            <input type="submit" class="form-control btn btn-primary" name="submit" value="Cari">
          </form>
        </div>
      </div>
    </div>
    <hr>
    <p class="text-right">
      <a class="btn btn-primary" href="<?php echo base_url('katalog')?>"><i class="fa fa-backward"></i> Kembali</a>
    </p>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <?php if (isset($buku->cover_buku)): ?>
          <img class="img img-responsive img-thumbnail" src="<?php echo base_url('assets/upload/image/'.$buku->cover_buku)?>" alt="<?php echo $buku->judul_buku?>">
        <?php else: ?>
          <p class=" alert alert-success text-warning">Tidak ada cover</p>
        <?php endif; ?>
      </div>
      <div class="col-md-8">

        <?php if (count($file_buku) < 1): ?>
          <p class="alert alert-success text-center"><i class="glyphicon glyphicon-warning-sign"></i>  File tidak tersedia</p>
        <?php else: ?>
          <!-- Table file buku -->
          <table class="table table-striped table-bordered table-hover" >
            <thead>
              <tr>
                <th>No.</th>
                <th>Judul File</th>
                <th>Deskripsi</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach ($file_buku as $file_buku): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $file_buku->judul_file ?></td>
                  <td><?php echo $file_buku->keterangan ?></td>
                  <td>
                     <a href="<?php echo base_url('katalog/baca_online/'.$file_buku->id_file_buku); ?>" class="btn btn-success btn-sm btn-block" target="_blank"><i class="fa fa-eye"></i> Baca Online </a>
                  </td>
                </tr>
                <?php $i++; endforeach; ?>
              </tbody>
            </table>
          <!-- END -->
        <?php endif; ?>
        <hr>
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th width="20%">Judul</th>
              <th><?php echo $buku->judul_buku ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Bahasa Buku</td>
              <td><?php echo $buku->nama_bahasa ?></td>
            </tr>
            <tr>
              <td>Jenis Buku</td>
              <td><?php echo $buku->nama_jenis ?></td>
            </tr>
            <tr>
              <td>Penulis Buku</td>
              <td><?php echo $buku->penulis_buku ?></td>
            </tr>
            <tr>
              <td>Subjek Buku</td>
              <td><?php echo $buku->subjek_buku ?></td>
            </tr>
            <tr>
              <td>Letak Buku</td>
              <td><?php echo $buku->letak_buku ?></td>
            </tr>
            <tr>
              <td>Kode Buku</td>
              <td><?php echo $buku->kode_buku ?></td>
            </tr>
            <tr>
              <td>Kolasi</td>
              <td><?php echo $buku->kolasi ?></td>
            </tr>
            <tr>
              <td>Penerbit</td>
              <td><?php echo $buku->penerbit ?></td>
            </tr>
            <tr>
              <td>Tahun Terbit</td>
              <td><?php echo $buku->tahun_terbit ?></td>
            </tr>
            <tr>
              <td>Nomor Seri</td>
              <td><?php echo $buku->nomor_seri ?></td>
            </tr>
            <tr>
              <td>Status Buku</td>
              <td><?php echo $buku->status_buku ?></td>
            </tr>
            <tr>
              <td>Deskripsi</td>
              <td class="text-justify"><?php echo $buku->ringkasan ?></td>
            </tr>
            <tr>
              <td>Jumlah Buku</td>
              <td><?php echo $buku->jumlah_buku ?></td>
            </tr>
            <tr>
              <td>Tanggal Entri</td>
              <td><?php echo $buku->tanggal_entri ?></td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
  </div>
</div>
