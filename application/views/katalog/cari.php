<div class="row">
  <div class="col-lg-12">
    <div class="sidebar-module sidebar-module-inset">
      <div class="panel">
        <div class="panel-body panel-success">
          <h2><?php echo $title ?></h2>
          <p class="alert alert-success">Hasil Pencarian dengan kata kunci : <strong><?php echo $keyword ?></strong></p>
          <form class="form-inline" action="<?php echo base_url('katalog')?>" method="post">
            <input type="text" class="form-control" name="cari" placeholder="Cari buku disini!" required>
            <input type="submit" class="form-control btn btn-primary" name="submit" value="Cari">
          </form>
        </div>
      </div>
    </div>

    <br><hr><br>

    <!-- Tabel Buku  -->
    <div class="col-md-12">

      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
          <tr>
            <th class="text-center">No.</th>
            <th class="text-center">Cover</th>
            <th class="text-center">Judul Buku</th>
            <th class="text-center">Penulis Buku</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach ($buku as $buku): ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td>
                <?php if (empty($buku->cover_buku)): ?>
                  Tidak Ada Cover
                <?php else: ?>
                  <img src="<?php echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku); ?>" class="img img-thumbnail" width="60">
                <?php endif; ?>
              </td>
              <td><?php echo $buku->judul_buku; ?></td>
              <td><?php echo $buku->penulis_buku; ?></td>
              <td class="text-center">
                <a href="<?php echo base_url('katalog/read/'.$buku->id_buku); ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Lihat Detail</a>
              </td>
            </tr>
            <?php $i++; endforeach; ?>
          </tbody>
        </table>
      </div>

  </div>
</div>
