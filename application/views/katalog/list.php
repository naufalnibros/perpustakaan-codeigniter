<hr class="featurette-divider">
<!-- Example row of columns -->
<div class="row">
  <div class="col-lg-8">
    <div class="sidebar-module sidebar-module-inset">
      <div class="panel">
        <div class="panel-body panel-success">
          <h2>Pencarian Buku</h2>
          <form class="form-inline" action="<?php echo base_url('katalog')?>" method="post">
            <input type="text" class="form-control" name="cari" placeholder="Cari judul Buku" required>
            <input type="submit" class="form-control btn btn-primary" name="submit" value="Cari">
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-body">
        <h2>Buku Baru</h2>

        <?php foreach ($buku as $buku): ?>
        <div class="row buku">
          <div class="col-md-4">
            <a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>">
              <img class="img-thumbnail img-rounded" src="<?php echo base_url('assets/upload/image/'.$buku->cover_buku)?>" alt="<?php echo $buku->judul_buku?>">
            </a>
          </div>
          <div class="col-md-8">
            <h4><a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>"><?php echo $buku->judul_buku ?></a></h4>
            <p><?php echo character_limiter($buku->ringkasan, 60); ?></p>
          </div>
          <div class="clearfix"></div>
        </div>
        <?php endforeach; ?>

        <p>
          <a class="btn btn-primary btn-block" href="<?php echo base_url('katalog')?>">Koleksi Buku</a>
        </p>

      </div>
    </div>
  </div>

</div>
