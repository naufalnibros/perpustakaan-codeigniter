<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <h2><?php echo $title ?></h2>
        <p>
          <a class="btn btn-success btn-block btn-lg" href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>"><i class="glyphicon glyphicon-blackboard"></i> Kembali</a>
        </p>
        <iframe src="<?php echo base_url('assets/upload/files/'.$file_buku->nama_file)?>" width="100%" height="800" allowfullscreen webkitallowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
