<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    // $this->load->model(array('kontak_model'));
  }

  public function index()
  {
    $data = array('title' => 'Hubungi Kami',
                  'isi'   => 'kontak/list');
    $this->load->view('layout/wrapper', $data);
  }

}
