<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('buku_model');
    $this->load->model('jenis_model');
    $this->load->model('bahasa_model');
    $this->load->model('file_buku_model');
  }

  public function index()
  {
    $buku = $this->buku_model->listing();

    $data = array('title' => 'Data Buku ('.count($buku).')',
                  'buku'  => $buku,
                  'isi'   => 'admin/buku/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Tambah Buku
  public function tambah()
  {
    $jenis  = $this->jenis_model->listing();
    $bahasa  = $this->bahasa_model->listing();
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('judul_buku','Judul Buku','required|is_unique[buku.judul_buku]',
                        array('required' => ' Judul Buku Harus Diisi',
                              'is_unique'=> ' Judul Buka Sudah Ada!'));


    $validasi->set_rules('penulis_buku','Penulis Buku','required', 'Penulis Buku Harus Diisi');


    if ($validasi->run()) {

      if (!empty($_FILES['cover_buku']['name'])) {
        $config['upload_path']   = './assets/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|svg|jpeg';
        $config['max_size']      = '12000'; // KB
        $this->upload->initialize($config);
        // $this->load->library('upload', $config);
        if(! $this->upload->do_upload('cover_buku')) {

          $data = array('title'  => 'Tambah Buku',
          'jenis'  => $jenis,
          'bahasa' => $bahasa,
          'error'  => $this->upload->display_errors(),
          'isi'    => 'admin/buku/tambah');
          $this->load->view('admin/layout/wrapper', $data);

        }else {

          $upload_data = array('uploads' =>$this->upload->data());
          // Image Editor
          $config['image_library']  	= 'gd2';
          $config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
          $config['new_image']     	= './assets/upload/image/thumbs/';
          $config['create_thumb']   	= TRUE;
          $config['quality']       	= "100%";
          $config['maintain_ratio']   = TRUE;
          $config['width']       		= 360; // Pixel
          $config['height']       	= 360; // Pixel
          $config['x_axis']       	= 0;
          $config['y_axis']       	= 0;
          $config['thumb_marker']   	= '';
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();

          $data = array(
            'id_user'        => $this->session->userdata('id_user'),
            'id_jenis'       => $this->input->post('id_jenis'),
            'id_bahasa'       => $this->input->post('id_bahasa'),
            'judul_buku'       => $this->input->post('judul_buku'),
            'penulis_buku'    => $this->input->post('penulis_buku'),
            'subjek_buku'    => $this->input->post('subjek_buku'),
            'letak_buku'    => $this->input->post('letak_buku'),
            'kode_buku'    => $this->input->post('kode_buku'),
            'kolasi'    => $this->input->post('kolasi'),
            'penerbit'    => $this->input->post('penerbit'),
            'tahun_terbit'    => $this->input->post('tahun_terbit'),
            'nomor_seri'    => $this->input->post('nomor_seri'),
            'status_buku'    => $this->input->post('status_buku'),
            'ringkasan'    => $this->input->post('ringkasan'),
            'cover_buku'    => $upload_data['uploads']['file_name'],
            'jumlah_buku' => $this->input->post('jumlah_buku'),
            'tanggal_entri'  => date('Y-m-d H:i:s'));

            $this->buku_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
            redirect(base_url('admin/buku'), 'refresh');
          }
        } else {
          $data = array(
          'id_user'        => $this->session->userdata('id_user'),
          'id_jenis'       => $this->input->post('id_jenis'),
          'id_bahasa'       => $this->input->post('id_bahasa'),
          'judul_buku'       => $this->input->post('judul_buku'),
          'penulis_buku'    => $this->input->post('penulis_buku'),
          'subjek_buku'    => $this->input->post('subjek_buku'),
          'letak_buku'    => $this->input->post('letak_buku'),
          'kode_buku'    => $this->input->post('kode_buku'),
          'kolasi'    => $this->input->post('kolasi'),
          'penerbit'    => $this->input->post('penerbit'),
          'tahun_terbit'    => $this->input->post('tahun_terbit'),
          'nomor_seri'    => $this->input->post('nomor_seri'),
          'status_buku'    => $this->input->post('status_buku'),
          'ringkasan'    => $this->input->post('ringkasan'),
          'jumlah_buku' => $this->input->post('jumlah_buku'),
          'tanggal_entri'  => date('Y-m-d H:i:s'));

          $this->buku_model->tambah($data);
          $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
          redirect(base_url('admin/buku'), 'refresh');

        }
      }

      $data = array('title'  => 'Tambah Buku',
      'jenis'  => $jenis,
      'bahasa' => $bahasa,
      'isi'    => 'admin/buku/tambah');
      $this->load->view('admin/layout/wrapper', $data);

    }

  // Edit Buku
  public function edit($id_buku)
  {
    $buku = $this->buku_model->detail($id_buku);
    $jenis  = $this->jenis_model->listing();
    $bahasa  = $this->bahasa_model->listing();
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('judul_buku','Judul Buku','required', array('required' => ' Judul Buku Harus Diisi'));


    $validasi->set_rules('penulis_buku','Penulis Buku','required', 'Penulis Buku Harus Diisi');


    if ($validasi->run()) {

      if (!empty($_FILES['cover_buku']['name'])) {
        $config['upload_path']   = './assets/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|svg|jpeg';
        $config['max_size']      = '12000'; // KB
        $this->upload->initialize($config);
        // $this->load->library('upload', $config);
        if(!$this->upload->do_upload('cover_buku')) {

          $data = array('title'  => 'Edit Buku '.$buku->judul_buku,
          'buku' => $buku,
          'jenis'  => $jenis,
          'bahasa' => $bahasa,
          'error'  => $this->upload->display_errors(),
          'isi'    => 'admin/buku/edit');
          $this->load->view('admin/layout/wrapper', $data);

        } else {

          $upload_data = array('uploads' =>$this->upload->data());
          // Image Editor
          $config['image_library']  	= 'gd2';
          $config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
          $config['new_image']     	= './assets/upload/image/thumbs/';
          $config['create_thumb']   	= TRUE;
          $config['quality']       	= "100%";
          $config['maintain_ratio']   = TRUE;
          $config['width']       		= 360; // Pixel
          $config['height']       	= 360; // Pixel
          $config['x_axis']       	= 0;
          $config['y_axis']       	= 0;
          $config['thumb_marker']   	= '';
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();

          if ($this->buku->cover_buku != "") {
            unlink('./assets/upload/images/'.$buku->cover_buku);
            unlink('./assets/upload/images/thumbs/'.$buku->cover_buku);
          }

          $data = array(
            'id_buku'        => $id_buku,
            'id_user'        => $this->session->userdata('id_user'),
            'id_jenis'       => $this->input->post('id_jenis'),
            'id_bahasa'       => $this->input->post('id_bahasa'),
            'judul_buku'       => $this->input->post('judul_buku'),
            'penulis_buku'    => $this->input->post('penulis_buku'),
            'subjek_buku'    => $this->input->post('subjek_buku'),
            'letak_buku'    => $this->input->post('letak_buku'),
            'kode_buku'    => $this->input->post('kode_buku'),
            'kolasi'    => $this->input->post('kolasi'),
            'penerbit'    => $this->input->post('penerbit'),
            'tahun_terbit'    => $this->input->post('tahun_terbit'),
            'nomor_seri'    => $this->input->post('nomor_seri'),
            'status_buku'    => $this->input->post('status_buku'),
            'ringkasan'    => $this->input->post('ringkasan'),
            'cover_buku'    => $upload_data['uploads']['file_name'],
            'jumlah_buku' => $this->input->post('jumlah_buku')
            // 'tanggal_entri'  => date('Y-m-d H:i:s')
          );

            $this->buku_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah di Update');
            redirect(base_url('admin/buku/edit/'.$id_buku));
          }
        } else {
          $data = array(
          'id_buku'        => $id_buku,
          'id_user'        => $this->session->userdata('id_user'),
          'id_jenis'       => $this->input->post('id_jenis'),
          'id_bahasa'       => $this->input->post('id_bahasa'),
          'judul_buku'       => $this->input->post('judul_buku'),
          'penulis_buku'    => $this->input->post('penulis_buku'),
          'subjek_buku'    => $this->input->post('subjek_buku'),
          'letak_buku'    => $this->input->post('letak_buku'),
          'kode_buku'    => $this->input->post('kode_buku'),
          'kolasi'    => $this->input->post('kolasi'),
          'penerbit'    => $this->input->post('penerbit'),
          'tahun_terbit'    => $this->input->post('tahun_terbit'),
          'nomor_seri'    => $this->input->post('nomor_seri'),
          'status_buku'    => $this->input->post('status_buku'),
          'ringkasan'    => $this->input->post('ringkasan'),
          'jumlah_buku' => $this->input->post('jumlah_buku')
          // 'tanggal_entri'  => date('Y-m-d H:i:s')
        );

          $this->buku_model->edit($data);
          $this->session->set_flashdata('sukses', 'Data telah di Update');
          redirect(base_url('admin/buku'));

        }
      }

      $data = array('title'  => 'Edit Buku '.$buku->judul_buku,
      'buku' => $buku,
      'jenis'  => $jenis,
      'bahasa' => $bahasa,
      'isi'    => 'admin/buku/edit');
      $this->load->view('admin/layout/wrapper', $data);

    }

  public function delete($id_buku)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $buku = $this->buku_model->detail($id_buku);

      if (isset($buku->cover_buku)) {
        unlink('./assets/upload/image/'.$buku->cover_buku);
        unlink('./assets/upload/image/thumbs/'.$buku->cover_buku);
      }

      $data = array('id_buku' => $id_buku);

      $this->buku_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/buku'), 'refresh');
    }
  }

}
