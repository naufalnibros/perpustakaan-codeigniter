<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('anggota_model');
  }

  public function index()
  {
    $anggota = $this->anggota_model->listing();

    $data = array('title' => 'Data Anggota ('.count($anggota).')',
                  'anggota'  => $anggota,
                  'isi'   => 'admin/anggota/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Tambah Anggota
  public function tambah()
  {
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_anggota','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('email', 'Email', 'required|valid_email',
                          array('required'    => 'Email Harus Diisi',
                                'valid_email' => 'Format Email tidak benar!'));

    $validasi->set_rules('username','Username','required|is_unique[anggota.username]',
                          array('required'  => 'Username Harus diisi',
                                'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah ada. Buat Username baru'));

    $validasi->set_rules('password','Password','required|min_length[6]',
                          array('required'    => 'Password Harus diisi',
                                'min_length'  => 'Password minimal 6 karakter'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Tambah Anggota',
                    'isi'   => 'admin/anggota/tambah');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      $data = array(
        'id_user'         => $this->session->userdata('id_user'),
        'status_anggota'  => $this->input->post('status_anggota'),
        'nama_anggota'    => $this->input->post('nama_anggota'),
        'email'           => $this->input->post('email'),
        'telepon'         => $this->input->post('telepon'),
        'instansi'        => $this->input->post('instansi'),
        'username'        => $this->input->post('username'),
        'password'        => sha1($this->input->post('password'))
      );

        $this->anggota_model->tambah($data);
        $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
        redirect(base_url('admin/anggota'), 'refresh');
    }
  }

  // Edit Anggota
  public function edit($id_anggota)
  {
    $anggota = $this->anggota_model->detail($id_anggota);

    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_anggota','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('email', 'Email', 'required|valid_email',
                          array('required'    => 'Email Harus Diisi',
                                'valid_email' => 'Format Email tidak benar!'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Edit Anggota ('.$anggota->nama_anggota.')',
                    'anggota'  => $anggota,
                    'isi'   => 'admin/anggota/edit');
      $this->load->view('admin/layout/wrapper', $data);
    }else {

      //jika input pass lebih dari 6 karakter
      if (strlen($this->input->post('password')) > 6) {
        $data = array(
          'id_anggota'      => $id_anggota,
          'id_user'         => $this->session->userdata('id_user'),
          'status_anggota'  => $this->input->post('status_anggota'),
          'nama_anggota'    => $this->input->post('nama_anggota'),
          'email'           => $this->input->post('email'),
          'telepon'         => $this->input->post('telepon'),
          'instansi'        => $this->input->post('instansi'),
          // 'username'        => $this->input->post('username'),
          'password'        => sha1($this->input->post('password'))
        );
      } else {
        $data = array(
          'id_anggota'      => $id_anggota,
          'id_user'         => $this->session->userdata('id_user'),
          'status_anggota'  => $this->input->post('status_anggota'),
          'nama_anggota'    => $this->input->post('nama_anggota'),
          'email'           => $this->input->post('email'),
          'telepon'         => $this->input->post('telepon'),
          'instansi'        => $this->input->post('instansi')
          // 'username'        => $this->input->post('username')
        );
      }

        $this->anggota_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diubah');
        redirect(base_url('admin/anggota'), 'refresh');
    }
  }

  public function delete($id_anggota)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses',' Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $data = array('id_anggota' => $id_anggota);

      $this->anggota_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/anggota'), 'refresh');
    }
  }

}
