<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('user_model');
  }

  // homepage
  function index()
  {
    $data = array('title' => 'Halaman Dashboard',
                  'isi'   => 'admin/dashboard/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Halaman Profil
  public function profile()
  {
    $id_user  = $this->session->userdata('id_user');
    $user = $this->user_model->detail($id_user);

    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('email', 'Email', 'required|valid_email',
                          array('required'    => 'Email Harus Diisi',
                                'valid_email' => 'Format Email tidak benar!'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Update Profile ('.$user->nama.')',
                    'user'  => $user,
                    'isi'   => 'admin/dashboard/profile');
      $this->load->view('admin/layout/wrapper', $data);
    }else {

      //jika input pass lebih dari 6 karakter
      if (strlen($this->input->post('password')) > 6) {
        $data = array(
          'id_user'     => $id_user,
          'nama'        => $this->input->post('nama'),
          'email'       => $this->input->post('email'),
          'password'    => sha1($this->input->post('password')),
          'akses_level' => $this->input->post('akses_level'),
          'keterangan'  => $this->input->post('keterangan'));
      } else {
        $data = array(
          'id_user'     => $id_user,
          'nama'        => $this->input->post('nama'),
          'email'       => $this->input->post('email'),
          'akses_level' => $this->input->post('akses_level'),
          'keterangan'  => $this->input->post('keterangan'));
      }

        $this->user_model->edit($data);
        $this->session->set_flashdata('sukses', 'Profile telah di Update');
        redirect(base_url('admin/dashboard/profile'), 'refresh');
    }
  }

}
