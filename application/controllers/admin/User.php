<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('user_model');
  }

  public function index()
  {
    $user = $this->user_model->listing();

    $data = array('title' => 'Data User ('.count($user).')',
                  'user'  => $user,
                  'isi'   => 'admin/user/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Tambah User
  public function tambah()
  {
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('email', 'Email', 'required|valid_email',
                          array('required'    => 'Email Harus Diisi',
                                'valid_email' => 'Format Email tidak benar!'));

    $validasi->set_rules('username','Username','required|is_unique[user.username]',
                          array('required'  => 'Username Harus diisi',
                                'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah ada. Buat Username baru'));

    $validasi->set_rules('password','Password','required|min_length[6]',
                          array('required'    => 'Password Harus diisi',
                                'min_length'  => 'Password minimal 6 karakter'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Tambah User',
                    'isi'   => 'admin/user/tambah');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      $data = array(
        'nama'        => $this->input->post('nama'),
        'email'       => $this->input->post('email'),
        'username'    => $this->input->post('username'),
        'password'    => sha1($this->input->post('password')),
        'akses_level' => $this->input->post('akses_level'),
        'keterangan'  => $this->input->post('keterangan'));

        $this->user_model->tambah($data);
        $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
        redirect(base_url('admin/user'), 'refresh');
    }
  }

  // Edit User
  public function edit($id_user)
  {
    $user = $this->user_model->detail($id_user);

    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('email', 'Email', 'required|valid_email',
                          array('required'    => 'Email Harus Diisi',
                                'valid_email' => 'Format Email tidak benar!'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Edit User ('.$user->nama.')',
                    'user'  => $user,
                    'isi'   => 'admin/user/edit');
      $this->load->view('admin/layout/wrapper', $data);
    }else {

      //jika input pass lebih dari 6 karakter
      if (strlen($this->input->post('password')) > 6) {
        $data = array(
          'id_user'     => $id_user,
          'nama'        => $this->input->post('nama'),
          'email'       => $this->input->post('email'),
          'password'    => sha1($this->input->post('password')),
          'akses_level' => $this->input->post('akses_level'),
          'keterangan'  => $this->input->post('keterangan'));
      } else {
        $data = array(
          'id_user'     => $id_user,
          'nama'        => $this->input->post('nama'),
          'email'       => $this->input->post('email'),
          'akses_level' => $this->input->post('akses_level'),
          'keterangan'  => $this->input->post('keterangan'));
      }

        $this->user_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diubah');
        redirect(base_url('admin/user'), 'refresh');
    }
  }

  public function delete($id_user)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $data = array('id_user' => $id_user);

      $this->user_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/user'), 'refresh');
    }
  }

}
