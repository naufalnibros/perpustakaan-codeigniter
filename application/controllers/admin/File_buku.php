<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_buku extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('buku_model');
    $this->load->model('file_buku_model');
  }

  public function index()
  {
    $file_buku = $this->file_buku_model->listing();

    $data = array('title' => 'Data File Buku ('.count($file_buku).')',
                  'file_buku'  => $file_buku,
                  'isi'   => 'admin/file_buku/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Unduh file buku
  public function unduh($id_file_buku){
    $file_buku = $this->file_buku_model->detail($id_file_buku);
    //Proses download
    $path      = 'assets/upload/files/';
    $file = $file_buku->nama_file;

    force_download($path.$file, NULL);

  }

  // Kelola File Buku
  public function kelola($id_buku)
  {
    $file_buku = $this->file_buku_model->buku($id_buku);
    $buku      = $this->buku_model->detail($id_buku);

    //validasi form
    $validasi = $this->form_validation;
    $validasi->set_rules('judul_file','Judul File','required', '%s Harus diisi');

    if ($validasi->run()) {
      $config['upload_path']   = './assets/upload/files/';
      $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
      $config['max_size']      = '12000'; // KB
      $this->upload->initialize($config);
      // $this->load->library('upload', $config);
      if(!$this->upload->do_upload('nama_file')) {
        // End Validasi

        $data = array('title' => 'Data File Buku : '.$buku->judul_buku.' ('.count($file_buku).' File)',
        'file_buku'  => $file_buku,
        'buku'  => $buku,
        'error' => $this->upload->display_errors(),
        'isi'   => 'admin/file_buku/list');
        $this->load->view('admin/layout/wrapper', $data);

      } else {

        $upload_data = array('uploads' =>$this->upload->data());

        $data = array(
          'id_user'         => $this->session->userdata('id_user'),
          'id_buku'         => $id_buku,
          'judul_file'      => $this->input->post('judul_file'),
          'nama_file' => $upload_data['uploads']['file_name'],
          'urutan'          => $this->input->post('urutan'),
          'keterangan'      => $this->input->post('keterangan')
        );

          $this->file_buku_model->tambah($data);
          $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
          redirect(base_url('admin/file_buku/kelola/'.$id_buku), 'refresh');
        }
      }

      $data = array(
        'title' => 'Data File Buku : '.$buku->judul_buku.' ('.count($file_buku).' File)',
        'file_buku'  => $file_buku,
        'buku'  => $buku,
        'isi'   => 'admin/file_buku/list');
      $this->load->view('admin/layout/wrapper', $data);
  }

  // Edit File Buku
  public function edit($id_file_buku){
    $file_buku = $this->file_buku_model->detail($id_file_buku);
    $id_buku   = $file_buku->id_buku;

    $buku      = $this->buku_model->detail($id_buku);

    //validasi form
    $validasi = $this->form_validation;
    $validasi->set_rules('judul_file','Judul File','required', '%s Harus diisi');

    if ($validasi->run()) {
      // Jika field upload tidak kosong
      if (!empty($_FILES['nama_file']['name'])) {
        # code...
      $config['upload_path']   = './assets/upload/files/';
      $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
      $config['max_size']      = '12000'; // KB
      $this->upload->initialize($config);
      // $this->load->library('upload', $config);
      if(!$this->upload->do_upload('nama_file')) {
        // End Validasi

        $data = array(
          'title' => 'Edit File Buku : '.$buku->judul_buku.' ('.count($file_buku).' File)',
          'file_buku'  => $file_buku,
          'buku'  => $buku,
          'error' => $this->upload->display_errors(),
          'isi'   => 'admin/file_buku/list'
        );
        $this->load->view('admin/layout/wrapper', $data);

      } else {

        $upload_data = array('uploads' =>$this->upload->data());

        // Hapus file lama
        unlink('assets/upload/files/'.$file_buku->nama_file);

        $data = array(
          'id_file_buku'    => $id_file_buku,
          'id_user'         => $this->session->userdata('id_user'),
          'id_buku'         => $id_buku,
          'judul_file'      => $this->input->post('judul_file'),
          'nama_file'       => $upload_data['uploads']['file_name'],
          'urutan'          => $this->input->post('urutan'),
          'keterangan'      => $this->input->post('keterangan')
        );
        $this->file_buku_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diupdate');
        redirect(base_url('admin/file_buku/kelola/'.$id_buku), 'refresh');
      }
    } else {
        // Jika field upload file tidak terisi
        $data = array(
          'id_file_buku'     => $id_file_buku,
          'id_user'         => $this->session->userdata('id_user'),
          'id_buku'         => $id_buku,
          'judul_file'      => $this->input->post('judul_file'),
          'urutan'          => $this->input->post('urutan'),
          'keterangan'      => $this->input->post('keterangan')
        );

        $this->file_buku_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diupdate');
        redirect(base_url('admin/file_buku/kelola/'.$id_buku), 'refresh');
      }
    }
    $data = array(
      'title' => 'Edit File Buku : '.$buku->judul_buku.' ('.count($file_buku).' File)',
      'file_buku'  => $file_buku,
      'buku'  => $buku,
      'isi'   => 'admin/file_buku/edit'
    );
    $this->load->view('admin/layout/wrapper', $data);
  }


  public function delete($id_file_buku, $id_buku)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $file_buku = $this->file_buku_model->detail($id_file_buku);

      if (isset($file_buku->nama_file)) {
        unlink('./assets/upload/files/'.$file_buku->nama_file);
      }

      $data = array('id_file_buku' => $id_file_buku);

      $this->file_buku_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/file_buku/kelola/'.$id_buku), 'refresh');
    }
  }

}
