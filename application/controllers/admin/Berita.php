<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('berita_model');
  }

  public function index()
  {
    $berita = $this->berita_model->listing();

    $data = array('title' => 'Data Berita ('.count($berita).')',
                  'berita'  => $berita,
                  'isi'   => 'admin/berita/list');

    $this->load->view('admin/layout/wrapper', $data);
  }

  // Tambah Berita
  public function tambah()
  {
    //validasi form
    $validasi = $this->form_validation;
    $validasi->set_rules('judul_berita','Judul Berita','required', '%s Harus diisi');
    $validasi->set_rules('isi','Isi Berita','required', '%s Harus diisi');

    if ($validasi->run()) {
      $config['upload_path']   = './assets/upload/image/';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['max_size']      = '12000'; // KB
      $this->upload->initialize($config);
      // $this->load->library('upload', $config);

      if(!$this->upload->do_upload('gambar')) {
        // End Validasi

        $data = array(
          'title' => 'Tambah Berita',
          'error' => $this->upload->display_errors(),
          'isi'   => 'admin/berita/tambah'
        );
        $this->load->view('admin/layout/wrapper', $data);

      } else {
        // Jika filed Upload gambar terisi
        $upload_data = array('uploads' =>$this->upload->data());
        // Image Editor
        $config['image_library']  	= 'gd2';
        $config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
        $config['new_image']     	= './assets/upload/image/thumbs/';
        $config['create_thumb']   	= TRUE;
        $config['quality']       	= "100%";
        $config['maintain_ratio']   = TRUE;
        $config['width']       		= 360; // Pixel
        $config['height']       	= 360; // Pixel
        $config['x_axis']       	= 0;
        $config['y_axis']       	= 0;
        $config['thumb_marker']   	= '';
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $slug_berita = url_title($this->input->post('judul_berita'), 'dash', TRUE);

        $data = array(
          'id_user'         => $this->session->userdata('id_user'),
          'slug_berita'         => $slug_berita,
          'judul_berita'      => $this->input->post('judul_berita'),
          'isi'      => $this->input->post('isi'),
          'gambar'          => $upload_data['uploads']['file_name'],
          'status_berita'          => $this->input->post('status_berita'),
          'jenis_berita'          => $this->input->post('jenis_berita')
        );

          $this->berita_model->tambah($data);
          $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
          redirect(base_url('admin/berita'), 'refresh');
        }
      }

      $data = array(
        'title' => 'Tambah Berita',
        'isi'   => 'admin/berita/tambah'
      );
      $this->load->view('admin/layout/wrapper', $data);
  }

  // Edit File Buku
  public function edit($id_berita){
    $berita = $this->berita_model->detail($id_berita);

    //validasi form
    $validasi = $this->form_validation;
    $validasi->set_rules('judul_berita', 'Judul Berita', 'required', '%s Harus di Isi');
    $validasi->set_rules('isi', 'Isi Berita', 'required', '%s Harus di Isi');

    if ($validasi->run()) {
      // Jika field upload tidak kosong
      if (!empty($_FILES['gambar']['name'])) {
        # code...
      $config['upload_path']   = './assets/upload/image/';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['max_size']      = '12000'; // KB
      $this->upload->initialize($config);
      // $this->load->library('upload', $config);
      if(!$this->upload->do_upload('gambar')) {
        // End Validasi

        $data = array(
          'title' => 'Edit Berita: '.$berita->judul_berita,
          'berita' => $berita,
          'error' => $this->upload->display_errors(),
          'isi'   => 'admin/berita/list'
        );
        $this->load->view('admin/layout/wrapper', $data);

      } else {

        $upload_data = array('uploads' =>$this->upload->data());

        // Image Editor
        $config['image_library']  	= 'gd2';
        $config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
        $config['new_image']     	= './assets/upload/image/thumbs/';
        $config['create_thumb']   	= TRUE;
        $config['quality']       	= "100%";
        $config['maintain_ratio']   = TRUE;
        $config['width']       		= 360; // Pixel
        $config['height']       	= 360; // Pixel
        $config['x_axis']       	= 0;
        $config['y_axis']       	= 0;
        $config['thumb_marker']   	= '';
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        if (isset($berita->gambar)) {
          unlink('./assets/upload/images/'.$berita->gambar);
          unlink('./assets/upload/images/thumbs/'.$berita->gambar);
        }

        $slug_berita = url_title($this->input->post('judul_berita'), 'undescore', TRUE);
        $data = array(
          'id_berita'       => $id_berita,
          'id_user'         => $this->session->userdata('id_user'),
          'slug_berita'         => $slug_berita,
          'judul_berita'      => $this->input->post('judul_berita'),
          'isi'      => $this->input->post('isi'),
          'gambar'          => $upload_data['uploads']['file_name'],
          'status_berita'          => $this->input->post('status_berita'),
          'jenis_berita'          => $this->input->post('jenis_berita')
        );

          $this->berita_model->edit($data);
          $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
          redirect(base_url('admin/berita'), 'refresh');

      }
    } else {
        // Jika field upload file tidak terisi
        $slug_berita = url_title($this->input->post('judul_berita'), 'undescore', TRUE);

        $data = array(
          'id_berita'       => $id_berita,
          'id_user'         => $this->session->userdata('id_user'),
          'slug_berita'         => $slug_berita,
          'judul_berita'      => $this->input->post('judul_berita'),
          'isi'      => $this->input->post('isi'),
          'status_berita'          => $this->input->post('status_berita'),
          'jenis_berita'          => $this->input->post('jenis_berita')
        );

          $this->berita_model->edit($data);
          $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
          redirect(base_url('admin/berita'), 'refresh');
      }
    }
    $data = array(
      'title' => 'Edit Berita: '.$berita->judul_berita,
      'berita' => $berita,
      'isi'   => 'admin/berita/edit'
    );
    $this->load->view('admin/layout/wrapper', $data);
  }


  public function delete($id_berita)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {

      if (isset($berita->gambar)) {
        unlink('./assets/upload/images/'.$berita->gambar);
        unlink('./assets/upload/images/thumbs/'.$berita->gambar);
      }

      $data = array('id_berita' => $id_berita);
      $this->berita_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/berita'), 'refresh');
    }
  }

}
