<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('bahasa_model');
  }
  // Halaman utama bahasa buku
  public function index()
  {
    $bahasa = $this->bahasa_model->listing();
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_bahasa','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('kode_bahasa','Kode Bahasa Buku','required|is_unique[bahasa.kode_bahasa]',
                          array('required'  => 'Kode Bahasa Buku Harus diisi',
                                'is_unique' => 'Kode Bahasa Buku <strong>'.$this->input->post('kode_bahasa').'</strong> Sudah ada. Buat Kode Bahasa Buku baru'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Kelola Bahasa Buku',
                    'bahasa'  => $bahasa,
                    'isi'   => 'admin/bahasa/list');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      // Tambah Buku
      $data = array(
        'kode_bahasa'        => $this->input->post('kode_bahasa'),
        'nama_bahasa'      => $this->input->post('nama_bahasa'),
        'keterangan'  => $this->input->post('keterangan'),
        'urutan'  => $this->input->post('urutan')
      );

        $this->bahasa_model->tambah($data);
        $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
        redirect(base_url('admin/bahasa'), 'refresh');
    }
  }

  // Edit Bahasa
  public function edit($id_bahasa)
  {
    $bahasa = $this->bahasa_model->detail($id_bahasa);

    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_bahasa','Nama Bahasa Buku','required', 'Nama Harus Diisi');

    $validasi->set_rules('kode_bahasa', 'Kode Bahasa Buku', 'required',
                          array('required'    => 'Kode Bahasa Buku harus Di isi'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Edit Bahasa ('.$bahasa->nama_bahasa.')',
                    'bahasa'  => $bahasa,
                    'isi'   => 'admin/bahasa/edit');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      //jika input pass lebih dari 6 karakter
      $data = array(
        'id_bahasa'       => $id_bahasa,
        'kode_bahasa'     => $this->input->post('kode_bahasa'),
        'nama_bahasa'     => $this->input->post('nama_bahasa'),
        'urutan'         => $this->input->post('urutan'),
        'keterangan'     => $this->input->post('keterangan'));

        $this->bahasa_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diubah');
        redirect(base_url('admin/bahasa'), 'refresh');
      }
    }

  public function delete($id_bahasa)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $data = array('id_bahasa' => $id_bahasa);

      $this->bahasa_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/bahasa'), 'refresh');
    }
  }

}
