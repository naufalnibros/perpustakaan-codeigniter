<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('jenis_model');
  }
  // Halaman utama jenis buku
  public function index()
  {
    $jenis = $this->jenis_model->listing();
    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_jenis','Nama','required', 'Nama Harus Diisi');

    $validasi->set_rules('kode_jenis','Kode Jenis Buku','required|is_unique[jenis.kode_jenis]',
                          array('required'  => 'Kode Jenis Buku Harus diisi',
                                'is_unique' => 'Kode Jenis Buku <strong>'.$this->input->post('kode_jenis').'</strong> Sudah ada. Buat Kode Jenis Buku baru'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Kelola Jenis Buku',
                    'jenis'  => $jenis,
                    'isi'   => 'admin/jenis/list');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      // Tambah Buku
      $data = array(
        'kode_jenis'        => $this->input->post('kode_jenis'),
        'nama_jenis'      => $this->input->post('nama_jenis'),
        'keterangan'  => $this->input->post('keterangan'),
        'urutan'  => $this->input->post('urutan')
      );

        $this->jenis_model->tambah($data);
        $this->session->set_flashdata('sukses', 'Data telah ditambahkan');
        redirect(base_url('admin/jenis'), 'refresh');
    }
  }

  // Edit Jenis
  public function edit($id_jenis)
  {
    $jenis = $this->jenis_model->detail($id_jenis);

    //validasi form
    $validasi = $this->form_validation;

    $validasi->set_rules('nama_jenis','Nama Jenis Buku','required', 'Nama Harus Diisi');

    $validasi->set_rules('kode_jenis', 'Kode Jenis Buku', 'required',
                          array('required'    => 'Kode Jenis Buku harus Di isi'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Edit Jenis ('.$jenis->nama_jenis.')',
                    'jenis'  => $jenis,
                    'isi'   => 'admin/jenis/edit');
      $this->load->view('admin/layout/wrapper', $data);
    }else {
      //jika input pass lebih dari 6 karakter
      $data = array(
        'id_jenis'       => $id_jenis,
        'kode_jenis'     => $this->input->post('kode_jenis'),
        'nama_jenis'     => $this->input->post('nama_jenis'),
        'urutan'         => $this->input->post('urutan'),
        'keterangan'     => $this->input->post('keterangan'));

        $this->jenis_model->edit($data);
        $this->session->set_flashdata('sukses', 'Data telah diubah');
        redirect(base_url('admin/jenis'), 'refresh');
      }
    }

  public function delete($id_jenis)
  {
    if ($this->session->userdata('username') == '' && $this->session->userdata('akses_level') == '') {
      $this->session->set_flashdata('sukses','Login terlebih dahulu');
      redirect(base_url('login'));

    } else {
      $data = array('id_jenis' => $id_jenis);

      $this->jenis_model->delete($data);
      $this->session->set_flashdata('sukses', ' Data telah Dihapus');
      redirect(base_url('admin/jenis'), 'refresh');
    }
  }

}
