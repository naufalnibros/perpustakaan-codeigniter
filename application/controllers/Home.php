<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('buku_model','berita_model','jenis_model','bahasa_model','file_buku_model'));
    //Codeigniter : Write Less Do More
  }

  public function index()
  {
    $slider = $this->berita_model->slider();
    $berita = $this->berita_model->berita();
    $buku   = $this->buku_model->buku();

    $data = array('title' => 'Sistem Informasi Perpustakaan Online',
                  'slider'=> $slider,
                  'berita'=> $berita,
                  'buku'  => $buku,
                  'isi'   => 'home/list');

    $this->load->view('layout/wrapper', $data);
  }

}
