<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('berita_model'));
    //Codeigniter : Write Less Do More
  }

  // Berita homepage
  public function index()
  {
    $berita = $this->berita_model->berita();

    $data = array('title' => 'Berita Terbaru',
                  'berita'=> $berita,
                  'isi'   => 'berita/list');
    $this->load->view('layout/wrapper', $data);
  }

  // Berita Homepage
  public function read($slug_berita) {
      $berita       = $this->berita_model->read($slug_berita);
      $list_berita  = $this->berita_model->list_berita();

      $data = array('title'       => $berita->judul_berita,
                    'berita'      => $berita,
                    'list_berita' => $list_berita,
                    'isi'         => 'berita/read');
      $this->load->view('layout/wrapper', $data);


  }

}
