<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usulan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('usulan_model'));
  }

  public function index()
  {
    // Validasi input
    $validasi = $this->form_validation;

    $validasi->set_rules('judul','Judul','required|min_length[15]',
    array(
      'required'    => '%s harus diisi',
      'min_length'  => '%s minimal 15 Karakter'));

    $validasi->set_rules('penulis','Penulis','required|min_length[6]',
    array(
      'required'    => '%s harus diisi',
      'min_length'  => '%s minimal 6 karakter'));

    $validasi->set_rules('penerbit','Nama Penerbit','required|min_length[10]',
    array(
      'required'    => '%s harus diisi',
      'min_length'  => '%s minimal 10 karakter'));

    $validasi->set_rules('nama_pengusul','Nama Pengusul','required|min_length[6]',
    array(
      'required'    => '%s harus diisi',
      'min_length'  => '%s minimal 6 karakter'));

    $validasi->set_rules('email_pengusul','Email','required|valid_email',
    array(
      'required'    => '%s harus diisi',
      'valid_email' => 'Email %s yang Anda masukkan tidak valid'));

    if ($validasi->run() === FALSE) {

      $data = array('title' => 'Usulan Buku Baru',
      'isi'   => 'usulan/list');
      $this->load->view('layout/wrapper', $data);

    } else {
      $data = array(
        'judul'        => $this->input->post('judul'),
        'penulis'      => $this->input->post('penulis'),
        'penerbit'     => $this->input->post('penerbit'),
        'keterangan'   => $this->input->post('keterangan'),
        'nama_pengusul'=> $this->input->post('nama_pengusul'),
        'email_pengusul'=> $this->input->post('email_pengusul'),
        'ip_address'    => $this->input->ip_address(),
        'status_usulan' => 'Baru',
        'tanggal_usulan' => date('Y-m-d H:i:s')
      );
      $this->usulan_model->tambah($data);
      $this->session->set_flashdata('sukses','Usulan Anda telah terkirim. Terima kasih atas usulannya untuk informasi lebih lanjut kami hubungi via email.');
      redirect(base_url('usulan'), 'refresh');
    }



  }

}
