<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('buku_model','berita_model','jenis_model','bahasa_model','file_buku_model'));
  }

  public function index()
  {
    $buku = $this->buku_model->buku();

    $validasi = $this->form_validation->set_rules('cari', 'Kata Kunci', 'required',
                                            array('required' => 'Kata kunci Harus diisi', ));

   if ($validasi->run()) {
     $cari    = strip_tags($this->input->post('cari'));
     $keyword = str_replace(' ', '-', $cari);
     redirect(base_url('katalog/cari/'.$keyword), 'refresh');
   }

    $data = array('title' => 'Katalog Buku',
                  'buku'  => $buku,
                  'isi'   => 'katalog/list');
    $this->load->view('layout/wrapper', $data);
  }

  public function cari($keywords)
  {
    $keywords = str_replace('-',' ',strip_tags($keywords));
    $buku     = $this->buku_model->cari($keywords);

    $data = array(
      'title'   => 'Hasil Pencarian : '.$keywords.' ('.count($buku).' buku)',
      'buku'    => $buku,
      'keyword' => $keywords,
      'isi'     => 'katalog/cari');
    $this->load->view('layout/wrapper', $data);
  }

  public function read($id_buku)
  {
    $buku       = $this->buku_model->read($id_buku);
    $file_buku  = $this->file_buku_model->buku($id_buku);

    $data = array(
      'title'     => $buku->judul_buku,
      'buku'      => $buku,
      'file_buku' => $file_buku,
      'isi'     => 'katalog/detail');
      $this->load->view('layout/wrapper', $data);
  }

  public function baca_online($id_file_buku) {
    $file_buku = $this->file_buku_model->detail($id_file_buku);
    // $id_buku   = $file_buku->id_buku;
    $buku   = $this->buku_model->read($file_buku->id_buku);

    $data = array(
      'title' => 'Baca Online : '.$buku->judul_buku.' - '.$file_buku->judul_file,
      'buku'  => $buku,
      'file_buku' => $file_buku,
      'isi'   => 'katalog/baca');
    $this->load->view('layout/wrapper', $data);
  }

}
