<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('buku_model','file_buku_model'));
  }

  public function index()
  {
    $buku   = $this->buku_model->baru(); //buku baru
    $data = array('title' => 'Katalog Buku Baru',
                  'buku'  => $buku,
                  'isi'   => 'buku/list');

    $this->load->view('layout/wrapper', $data);

  }

}
