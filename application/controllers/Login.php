<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('user_model');
    //Codeigniter : Write Less Do More
  }

  // Halaman login
  function index()
  {
    $validasi = $this->form_validation;

    $validasi->set_rules('username','Username','required',
                        array('required' => 'Username harus disi!'));

    $validasi->set_rules('password','Password','required|min_length[6]',
                        array('required'    => 'Password harus disi!',
                              'min_length'  => 'Password minimal 6 karakter!'));

    if ($validasi->run() === FALSE) {
      $data = array('title' => 'Login Administrator', );
      $this->load->view('admin/v_login', $data);
    } else {
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      $cek_login = $this->user_model->login($username, $password);

      // Cek username di database
      if (count($cek_login) === 1) {
          $this->session->set_userdata('username',$username);
          $this->session->set_userdata('akses_level',$cek_login->akses_level);
          $this->session->set_userdata('id_user', $cek_login->id_user);
          $this->session->set_userdata('nama',$cek_login->nama);
          redirect(base_url('admin/dashboard'), 'refresh');
      }else {
        $this->session->set_flashdata('sukses', 'Username atau password tidak cocok');
        redirect(base_url('login'));
      }
    }
  }

  // Logout
  public function logout()
  {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('akses_level');
    $this->session->unset_userdata('id_user');
    $this->session->unset_userdata('nama');
    $this->session->set_flashdata('sukses', 'Anda Berhasil logout');
    redirect(base_url('login'), 'refresh');

  }

}
