<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usulan_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  //tambah Usulan
  public function tambah($data){
    $this->db->insert('usulan', $data);
  }

  //listing
  public function listing(){
    $this->db->select('*');
    $this->db->from('usulan');
    $this->db->order_by('id_usulan','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  // Detail
  public function detail($id_usulan)
  {
    $this->db->select('*');
    $this->db->from('usulan');
    $this->db->where('id_usulan', $id_usulan);
    $this->db->order_by('id_usulan', 'DESC');

    $query = $this->db->get();
    return $query->row();
  }

  // Edit
  public function Edit($data)
  {
    $this->db->where('id_usulan',$data['id_usulan']);
    $this->db->update('usulan', $data);
  }

  public function delete($data)
  {
    $this->db->where('id_usulan',$data['id_usulan']);
    $this->db->delete('usulan', $data);
  }

  // Login
  public function login($username, $password)
  {
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where(array( 'username'    => $username,
                            'password'    => sha1($password)));
    // $this->db->order_by('id_user', 'DESC');

    $query = $this->db->get();
    return $query->row();
  }

}
