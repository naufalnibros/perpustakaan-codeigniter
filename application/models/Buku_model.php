<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  //tambah Buku
  public function tambah($data){
    $this->db->insert('buku', $data);
  }

  //listing
  public function listing(){
    $this->db->select('buku.*,
                       jenis.kode_jenis,
                       jenis.nama_jenis,
                       bahasa.kode_bahasa,
                       bahasa.nama_bahasa,
                       user.nama');
    $this->db->from('buku');
    // JOIN
    $this->db->join('jenis', 'jenis.id_jenis = buku.id_jenis', 'LEFT');
    $this->db->join('bahasa', 'bahasa.id_bahasa = buku.id_bahasa', 'LEFT');
    $this->db->join('user', 'user.id_user = buku.id_user', 'LEFT');

    $this->db->order_by('id_buku','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  // Buku
  public function buku(){
    $this->db->select('buku.*,
                       jenis.kode_jenis,
                       jenis.nama_jenis,
                       bahasa.kode_bahasa,
                       bahasa.nama_bahasa,
                       user.nama');
    $this->db->from('buku');
    // JOIN
    $this->db->join('jenis', 'jenis.id_jenis = buku.id_jenis', 'LEFT');
    $this->db->join('bahasa', 'bahasa.id_bahasa = buku.id_bahasa', 'LEFT');
    $this->db->join('user', 'user.id_user = buku.id_user', 'LEFT');

    // Where -> status_buku -> publish
    $this->db->where('status_buku', 'Publish');
    $this->db->order_by('id_buku','DESC');
    // limit 5
    $this->db->limit(5);

    $query = $this->db->get();
    return $query->result();
  }

  // Home Buku Baru -> controller Buku.php
  public function baru(){
    $this->db->select('buku.*,
                       jenis.kode_jenis,
                       jenis.nama_jenis,
                       bahasa.kode_bahasa,
                       bahasa.nama_bahasa,
                       user.nama');
    $this->db->from('buku');
    // JOIN
    $this->db->join('jenis', 'jenis.id_jenis = buku.id_jenis', 'LEFT');
    $this->db->join('bahasa', 'bahasa.id_bahasa = buku.id_bahasa', 'LEFT');
    $this->db->join('user', 'user.id_user = buku.id_user', 'LEFT');

    // Where -> status_buku -> publish
    $this->db->where('status_buku', 'Publish');
    $this->db->order_by('id_buku','DESC');
    // limit 5
    $this->db->limit(20);

    $query = $this->db->get();
    return $query->result();
  }

  // Pencarian Buku
  public function cari($keywords){
    $this->db->select('buku.*,
                       jenis.kode_jenis,
                       jenis.nama_jenis,
                       bahasa.kode_bahasa,
                       bahasa.nama_bahasa,
                       user.nama');
    $this->db->from('buku');
    // JOIN
    $this->db->join('jenis', 'jenis.id_jenis = buku.id_jenis', 'LEFT');
    $this->db->join('bahasa', 'bahasa.id_bahasa = buku.id_bahasa', 'LEFT');
    $this->db->join('user', 'user.id_user = buku.id_user', 'LEFT');

    // Where -> status_buku -> publish
    $this->db->where('status_buku', 'Publish');
    // Like
    $this->db->like('buku.judul_buku', $keywords);
    $this->db->order_by('id_buku','DESC');
    // limit 5
    $this->db->limit(5);

    $query = $this->db->get();
    return $query->result();
  }

  // Detail Buku -> perpus/read/id_buku
  public function read($id_buku){
    $this->db->select('buku.*,
                       jenis.kode_jenis,
                       jenis.nama_jenis,
                       bahasa.kode_bahasa,
                       bahasa.nama_bahasa,
                       user.nama');
    $this->db->from('buku');
    // JOIN
    $this->db->join('jenis', 'jenis.id_jenis = buku.id_jenis', 'LEFT');
    $this->db->join('bahasa', 'bahasa.id_bahasa = buku.id_bahasa', 'LEFT');
    $this->db->join('user', 'user.id_user = buku.id_user', 'LEFT');

    // Where -> status_buku -> publish
    $this->db->where('status_buku', 'Publish');
    // Where id_buku
    $this->db->where('id_buku', $id_buku);

    $query = $this->db->get();
    return $query->row();
  }

  // Detail
  public function detail($id_buku)
  {
    $this->db->select('*');
    $this->db->from('buku');
    $this->db->where('id_buku', $id_buku);
    $this->db->order_by('id_buku', 'DESC');

    $query = $this->db->get();
    return $query->row();
  }

  // Edit
  public function Edit($data)
  {
    $this->db->where('id_buku',$data['id_buku']);
    $this->db->update('buku', $data);
  }

  public function delete($data)
  {
    $this->db->where('id_buku',$data['id_buku']);
    $this->db->delete('buku', $data);
  }

  // Login
  public function login($username, $password)
  {
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where(array( 'username'    => $username,
                            'password'    => sha1($password)));
    // $this->db->order_by('id_buku', 'DESC');

    $query = $this->db->get();
    return $query->row();
  }

}
