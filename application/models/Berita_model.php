<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  //tambah User
  public function tambah($data){
    $this->db->insert('berita', $data);
  }

  //listing
  public function listing(){
    $this->db->select('*');
    $this->db->from('berita');
    $this->db->order_by('id_berita','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  //Banner Slider
  public function slider(){
    $this->db->select('*');
    $this->db->from('berita');
    // where Jenis_berita -> slider
    $this->db->where(array('jenis_berita'   => 'Slider',
                           'status_berita'  => 'Publish'));
    $this->db->order_by('id_berita','DESC');
    // Limit
    $this->db->limit(5);
    $query = $this->db->get();
    return $query->result();
  }

  //Berita
  public function berita(){
    $this->db->select('*');
    $this->db->from('berita');
    // where Jenis_berita -> slider
    $this->db->where(array('jenis_berita'   => 'Berita',
                           'status_berita'  => 'Publish'));
    $this->db->order_by('id_berita','DESC');
    // Limit
    $this->db->limit(1);
    $query = $this->db->get();
    return $query->row();
  }

  // List Berita -> view-> berita/read.php
  public function list_berita(){
    $this->db->select('*');
    $this->db->from('berita');
    // where Jenis_berita -> slider
    $this->db->where(array('status_berita'  => 'Publish'));
    $this->db->order_by('id_berita','DESC');
    // Limit
    $this->db->limit(10);
    $query = $this->db->get();
    return $query->result();
  }

  // Read berita
  public function read($slug_berita) {
    $this->db->select('*');
    $this->db->from('berita');
    // Where $slug_berita
    $this->db->where(array(
      'status_berita'=> 'Publish',
      'slug_berita'  => $slug_berita
    ));
    $query = $this->db->get();
    return $query->row();
  }

  // Detail
  public function detail($id_berita)
  {
    $this->db->select('*');
    $this->db->from('berita');
    $this->db->where('id_berita', $id_berita);
    $this->db->order_by('id_berita', 'DESC');

    $query = $this->db->get();
    return $query->row();
  }

  // Edit
  public function Edit($data)
  {
    $this->db->where('id_berita',$data['id_berita']);
    $this->db->update('berita', $data);
  }

  public function delete($data)
  {
    $this->db->where('id_berita',$data['id_berita']);
    $this->db->delete('berita', $data);
  }

}
